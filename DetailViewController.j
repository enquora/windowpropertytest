@import <Foundation/Foundation.j>
@import <AppKit/AppKit.j>

@implementation DetailViewController : CPViewController
{
}

- (id)init
{
    if(self = [super init])
    {
    }
    return self;
}

- (void)viewDidAppear
{
    var detailView = [self view];
    var detailWindow = [detailView window];
    console.log('viewDidAppear', detailView, detailWindow);
}
@end
